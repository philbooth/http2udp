# History

## 0.3.4

* Prevent large bodies from being sent.

## 0.3.3

* Prevent charset breaking content-type check.

## 0.3.2

* Update dependency on `check-types`.
* Fix lint errors.

## 0.3.1

* Limit incoming requests to application/x-www-form-urlencoded and text/plain.
* Saner error handling for badly formed request bodies.

## 0.3.0

* Implement simple [CORS] requests with `Access-Control-Allow-Origin` header.
* Add `--origin` command line option.

## 0.2.0

* Make logging function an option.
* Add `--silent` command line option.

## 0.1.1

* Unit tests.
* Fix broken call to `response.setHeader()`.
* Improve readme.

## 0.1.0

* First version.

[cors]: http://www.w3.org/TR/cors

