# http2udp

[![Build status][ci-image]][ci-status]

Forwards HTTP requests
to UDP sockets.

* [What's the point?](#whats-the-point)
* [How does it work?](#how-does-it-work)
* [How do I install it?](#how-do-i-install-it)
* [How do I use it?](#how-do-i-use-it)
    * [From the command line](#from-the-command-line)
    * [From a Node.js project](#from-a-nodejs-project)
* [Is there a change log?](#is-there-a-change-log)
* [How do I set up the build environment?](#how-do-i-set-up-the-build-environment)
* [What license is it released under?](#what-license-is-it-released-under)

## What's the point?

At the time of writing,
there is no way
to send UDP packets
from client-side JavaScript
running in a web browser.
That might seem a strange thing
to want to do,
but I do have a legitimate use case (I think).

UDP is a common protocol choice for monitoring systems,
where the issues of dropped packets and
preserving transmission order
are typically of lesser importance
than the ability to log
large numbers of events
with minimal overhead.
In my case,
I need to send client-side [RUM data][rum]
to a UDP monitoring server,
but crucially the pages that I am monitoring
are being served by a number of different back-end platforms.

Rather than adding
a monitoring endpoint
to every back-end,
it is far simpler for me
to create a separate server
that can act as a unified proxy,
reponsible for sending all of my monitoring data over UDP.

## How does it work?

http2udp is a very basic HTTP server
that runs on [Node.js][node]
and forwards requests to UDP sockets,
based on the parameters of the request.

## How do I install it?

Assuming that you already have Node.js and [npm],
you can install it globally like so:

```
sudo npm install -g http2udp
```

Or add it to your project's dependencies
in `package.json`
and run:

```
npm install
```

## How do I use it?

### From the command line

To see a list of command line options,
you can run:

```
http2udp --help
```

Available options are:

* `--host <hostname>`:
  Hostname to accept HTTP connections on.
  The default is 0.0.0.0 (INADDR_ANY).

* `--port <port>`:
  Port to accept HTTP connections on.
  The default is 8008.

* `--path <path>`:
  URL path to accept requests to.
  The default is /udp.

* `--origin <origin>`:
  Allowed origin(s) for cross-domain requests,
  comma separated.
  The default is *,
  specify null to force same origin.

* `--maxSize <characters>`:
  Maximum request body size in characters.
  The default is 1024.

* `--silent`:
  Prevent the command
  from logging output
  to the console.

If the server is running in the foreground,
you can terminate it with ^C.

If it is running in the background,
find the pid with `ps`
and `kill -9` it,
or just `killall http2udp`.

### From a Node.js project

```javascript
var http2udp = require('http2udp');

http2udp.listen({
    host: '127.0.0.1',            // Defaults to '0.0.0.0' (INADDR_ANY)
    port: 80,                     // Defaults to 8008
    path: '/foo/bar',             // Defaults to '/udp'
    origin: 'http://example.com', // Defaults to '*', may be an array or null
    maxSize: 1048576              // Defaults to 1024
    log: console.log              // Defaults to `function () {}`
});
```

## Is there a change log?

[Yes][history].

## How do I set up the build environment?

To install dependencies:

```
npm install
```

To lint the code:

```
npm run lint
```

To run the unit tests:

```
npm test
```

## What license is it released under?

[MIT][license]

[ci-image]: https://secure.travis-ci.org/philbooth/http2udp.png?branch=master
[ci-status]: http://travis-ci.org/#!/philbooth/http2udp
[rum]: http://en.wikipedia.org/wiki/Real_user_monitoring
[node]: http://nodejs.org/
[npm]: https://npmjs.org/
[history]: https://github.com/philbooth/http2udp/blob/master/HISTORY.md
[license]: https://github.com/philbooth/http2udp/blob/master/COPYING

