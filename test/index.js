'use strict';

var assert, mockery, spooks, modulePath, nop;

assert = require('chai').assert;
mockery = require('mockery');
spooks = require('spooks');

modulePath = '../src';

nop = function () {};

mockery.registerAllowable(modulePath);
mockery.registerAllowable('check-types');
mockery.registerAllowable('querystring');

suite('http2udp:', function () {
    var log;

    setup(function () {
        log = {};
        mockery.enable({ useCleanCache: true });
        mockery.registerMock('http', spooks.obj({
            archetype: { createServer: nop, listen: nop },
            log: log,
            chains: { createServer: true }
        }));
        mockery.registerMock('dgram', spooks.obj({
            archetype: { createSocket: nop },
            log: log,
            results: {
                createSocket: spooks.obj({
                    archetype: { send: nop, close: nop },
                    log: log
                })
            }
        }));
    });

    teardown(function () {
        mockery.deregisterMock('dgram');
        mockery.deregisterMock('http');
        mockery.disable();
        log = undefined;
    });

    test('require does not throw', function () {
        assert.doesNotThrow(function () {
            require(modulePath);
        });
    });

    test('require returns object', function () {
        assert.isObject(require(modulePath));
    });

    suite('require:', function () {
        var http2udp;

        setup(function () {
            http2udp = require(modulePath);
        });

        teardown(function () {
            http2udp = undefined;
        });

        test('listen function is exported', function () {
            assert.isFunction(http2udp.listen);
        });

        test('listen does not throw without options', function () {
            assert.doesNotThrow(function () {
                http2udp.listen();
            });
        });

        test('listen throws if host is empty string', function () {
            assert.throws(function () {
                http2udp.listen({
                    host: '',
                    port: 80,
                    path: '/foo',
                    maxSize: 1048576
                });
            });
        });

        test('listen throws if port is string', function () {
            assert.throws(function () {
                http2udp.listen({
                    host: '127.0.0.1',
                    port: '80',
                    path: '/foo',
                    maxSize: 1048576
                });
            });
        });

        test('listen throws if path is empty string', function () {
            assert.throws(function () {
                http2udp.listen({
                    host: '127.0.0.1',
                    port: 80,
                    path: '',
                    maxSize: 1048576
                });
            });
        });

        test('listen throws if maxSize is string', function () {
            assert.throws(function () {
                http2udp.listen({
                    host: '127.0.0.1',
                    port: 80,
                    path: '/foo',
                    maxSize: '1048576'
                });
            });
        });

        test('listen does not throw if options are valid', function () {
            assert.doesNotThrow(function () {
                http2udp.listen({
                    host: '127.0.0.1',
                    port: 80,
                    path: '/foo',
                    maxSize: 1048576
                });
            });
        });

        test('listen does not throw if options are null', function () {
            assert.doesNotThrow(function () {
                http2udp.listen({
                    host: null,
                    port: null,
                    path: null,
                    maxSize: null
                });
            });
        });

        suite('call listen with default options:', function () {
            setup(function () {
                http2udp.listen();
            });

            test('http.createServer was called once', function () {
                assert.strictEqual(log.counts.createServer, 1);
            });

            test('http.createServer was called correctly', function () {
                assert.lengthOf(log.args.createServer[0], 1);
                assert.isFunction(log.args.createServer[0][0]);
            });

            test('http.listen was called once', function () {
                assert.strictEqual(log.counts.listen, 1);
            });

            test('http.listen was called correctly', function () {
                assert.lengthOf(log.args.listen[0], 2);
                assert.strictEqual(log.args.listen[0][0], 8008);
                assert.strictEqual(log.args.listen[0][1], '0.0.0.0');
            });

            suite('invalid path:', function () {
                var request, response;

                setup(function () {
                    request = {
                        url: '/foo',
                        method: 'POST',
                        headers: {
                            'content-type': 'application/x-www-form-urlencoded'
                        }
                    };
                    response = spooks.obj({
                        archetype: { setHeader: nop, end: nop },
                        log: log
                    });
                    log.args.createServer[0][0](request, response);
                });

                teardown(function () {
                    request = response = undefined;
                });

                test('response.setHeader was called twice', function () {
                    assert.strictEqual(log.counts.setHeader, 2);
                });

                test('response.setHeader was called correctly first time', function () {
                    assert.strictEqual(log.these.setHeader[0], response);
                    assert.lengthOf(log.args.setHeader[0], 2);
                    assert.strictEqual(log.args.setHeader[0][0], 'Content-Type');
                    assert.strictEqual(log.args.setHeader[0][1], 'application/json');
                });

                test('response.setHeader was called correctly second time', function () {
                    assert.strictEqual(log.these.setHeader[1], response);
                    assert.lengthOf(log.args.setHeader[1], 2);
                    assert.strictEqual(log.args.setHeader[1][0], 'Access-Control-Allow-Origin');
                    assert.strictEqual(log.args.setHeader[1][1], '*');
                });

                test('response.end was called once', function () {
                    assert.strictEqual(log.counts.end, 1);
                });

                test('response.end was called correctly', function () {
                    assert.strictEqual(log.these.end[0], response);
                    assert.lengthOf(log.args.end[0], 1);
                    assert.strictEqual(log.args.end[0][0], '{ "error": "Invalid path `/foo`" }');
                });

                test('response.statusCode was set correctly', function () {
                    assert.strictEqual(response.statusCode, 404);
                });

                test('udp.createSocket was not called', function () {
                    assert.strictEqual(log.counts.createSocket, 0);
                });

                test('socket.send was not called', function () {
                    assert.strictEqual(log.counts.send, 0);
                });
            });

            suite('invalid method:', function () {
                var request, response;

                setup(function () {
                    request = {
                        url: '/udp',
                        method: 'PUT',
                        headers: {
                            'content-type': 'application/x-www-form-urlencoded'
                        }
                    };
                    response = spooks.obj({
                        archetype: { setHeader: nop, end: nop },
                        log: log
                    });
                    log.args.createServer[0][0](request, response);
                });

                teardown(function () {
                    request = response = undefined;
                });

                test('response.setHeader was called twice', function () {
                    assert.strictEqual(log.counts.setHeader, 2);
                });

                test('response.setHeader was called correctly first time', function () {
                    assert.strictEqual(log.args.setHeader[0][0], 'Content-Type');
                    assert.strictEqual(log.args.setHeader[0][1], 'application/json');
                });

                test('response.setHeader was called correctly second time', function () {
                    assert.strictEqual(log.args.setHeader[1][0], 'Access-Control-Allow-Origin');
                    assert.strictEqual(log.args.setHeader[1][1], '*');
                });

                test('response.end was called once', function () {
                    assert.strictEqual(log.counts.end, 1);
                });

                test('response.end was called correctly', function () {
                    assert.strictEqual(log.args.end[0][0], '{ "error": "Invalid method `PUT`" }');
                });

                test('response.statusCode was set correctly', function () {
                    assert.strictEqual(response.statusCode, 405);
                });
            });

            suite('invalid content type:', function () {
                var request, response;

                setup(function () {
                    request = {
                        url: '/udp',
                        method: 'POST',
                        headers: {
                            'content-type': 'application/json'
                        }
                    };
                    response = spooks.obj({
                        archetype: { setHeader: nop, end: nop },
                        log: log
                    });
                    log.args.createServer[0][0](request, response);
                });

                teardown(function () {
                    request = response = undefined;
                });

                test('response.setHeader was called twice', function () {
                    assert.strictEqual(log.counts.setHeader, 2);
                });

                test('response.end was called once', function () {
                    assert.strictEqual(log.counts.end, 1);
                });

                test('response.end was called correctly', function () {
                    assert.strictEqual(log.args.end[0][0], '{ "error": "Invalid content type `application/json`" }');
                });

                test('response.statusCode was set correctly', function () {
                    assert.strictEqual(response.statusCode, 415);
                });
            });

            suite('valid request:', function () {
                var request, response;

                setup(function () {
                    request = {
                        url: '/udp',
                        method: 'POST',
                        on: spooks.fn({
                            name: 'on',
                            log: log
                        }),
                        socket: {
                            destroy: spooks.fn({
                                name: 'destroy',
                                log: log
                            })
                        },
                        headers: {
                            'content-type': 'application/x-www-form-urlencoded'
                        }
                    };
                    response = spooks.obj({
                        archetype: { setHeader: nop, end: nop },
                        log: log
                    });
                    log.args.createServer[0][0](request, response);
                });

                teardown(function () {
                    request = response = undefined;
                });

                test('response.setHeader was called twice', function () {
                    assert.strictEqual(log.counts.setHeader, 2);
                });

                test('response.setHeader was called correctly first time', function () {
                    assert.strictEqual(log.args.setHeader[0][0], 'Content-Type');
                    assert.strictEqual(log.args.setHeader[0][1], 'application/json');
                });

                test('response.setHeader was called correctly second time', function () {
                    assert.strictEqual(log.args.setHeader[1][0], 'Access-Control-Allow-Origin');
                    assert.strictEqual(log.args.setHeader[1][1], '*');
                });

                test('request.on was called twice', function () {
                    assert.strictEqual(log.counts.on, 2);
                });

                test('request.on was called correctly first time', function () {
                    assert.lengthOf(log.args.on[0], 2);
                    assert.strictEqual(log.args.on[0][0], 'data');
                    assert.isFunction(log.args.on[0][1]);
                });

                test('request.on was called correctly second time', function () {
                    assert.lengthOf(log.args.on[1], 2);
                    assert.strictEqual(log.args.on[1][0], 'end');
                    assert.isFunction(log.args.on[1][1]);
                });

                suite('receive 51 characters of data:', function () {
                    setup(function () {
                        log.args.on[0][1]('port=42&address=address%20foo&payload=payload%20bar');
                    });

                    test('request.socket.destroy was not called', function () {
                        assert.strictEqual(log.counts.destroy, 0);
                    });

                    test('response.end was not called', function () {
                        assert.strictEqual(log.counts.end, 0);
                    });

                    suite('receive 973 characters of data:', function () {
                        setup(function () {
                            var i, data = '';
                            for (i = 0; i < 973; i += 1) {
                                data += ' ';
                            }
                            log.args.on[0][1](data);
                        });

                        test('request.socket.destroy was not called', function () {
                            assert.strictEqual(log.counts.destroy, 0);
                        });

                        test('response.end was not called', function () {
                            assert.strictEqual(log.counts.end, 0);
                        });

                        test('udp.createSocket was not called', function () {
                            assert.strictEqual(log.counts.createSocket, 0);
                        });

                        test('socket.send was not called', function () {
                            assert.strictEqual(log.counts.send, 0);
                        });

                        suite('receive one character of data:', function () {
                            setup(function () {
                                log.args.on[0][1](' ');
                            });

                            test('request.socket.destroy called once', function () {
                                assert.strictEqual(log.counts.destroy, 1);
                            });

                            test('request.socket.destroy was called correctly', function () {
                                assert.strictEqual(log.these.destroy[0], request.socket);
                                assert.lengthOf(log.args.destroy[0], 0);
                            });

                            test('response.end was called once', function () {
                                assert.strictEqual(log.counts.end, 1);
                            });

                            test('response.end was called correctly', function () {
                                assert.strictEqual(log.args.end[0][0], '{ "error": "Body too large" }');
                            });

                            test('response.statusCode was set correctly', function () {
                                assert.strictEqual(response.statusCode, 413);
                            });
                        });
                    });

                    suite('receive 1024 characters of data:', function () {
                        setup(function () {
                            var i, data = '';
                            for (i = 0; i < 1024; i += 1) {
                                data += ' ';
                            }
                            log.args.on[0][1](data);
                        });

                        test('request.socket.destroy called once', function () {
                            assert.strictEqual(log.counts.destroy, 1);
                        });

                        test('response.end was called once', function () {
                            assert.strictEqual(log.counts.end, 1);
                        });
                    });

                    suite('end data:', function () {
                        setup(function () {
                            log.args.on[1][1]();
                        });

                        test('udp.createSocket was called once', function () {
                            assert.strictEqual(log.counts.createSocket, 1);
                        });

                        test('udp.createSocket was called correctly', function () {
                            assert.lengthOf(log.args.createSocket[0], 1);
                            assert.strictEqual(log.args.createSocket[0][0], 'udp4');
                        });

                        test('socket.send was called once', function () {
                            assert.strictEqual(log.counts.send, 1);
                        });

                        test('socket.send was called correctly', function () {
                            assert.lengthOf(log.args.send[0], 6);
                            assert.instanceOf(log.args.send[0][0], Buffer);
                            assert.strictEqual(log.args.send[0][0].toString(), 'payload bar');
                            assert.strictEqual(log.args.send[0][1], 0);
                            assert.strictEqual(log.args.send[0][2], 11);
                            assert.strictEqual(log.args.send[0][3], 42);
                            assert.strictEqual(log.args.send[0][4], 'address foo');
                            assert.isFunction(log.args.send[0][5]);
                        });

                        test('socket.close was not called', function () {
                            assert.strictEqual(log.counts.close, 0);
                        });

                        test('response.end was not called', function () {
                            assert.strictEqual(log.counts.end, 0);
                        });

                        suite('error:', function () {
                            setup(function () {
                                log.args.send[0][5]('wibble');
                            });

                            test('socket.close was called once', function () {
                                assert.strictEqual(log.counts.close, 1);
                            });

                            test('socket.close was called correctly', function () {
                                assert.lengthOf(log.args.close[0], 0);
                            });

                            test('response.end was called once', function () {
                                assert.strictEqual(log.counts.end, 1);
                            });

                            test('response.end was called correctly', function () {
                                assert.strictEqual(log.args.end[0][0], '{ "error": "wibble" }');
                            });

                            test('response.statusCode was set correctly', function () {
                                assert.strictEqual(response.statusCode, 502);
                            });
                        });

                        suite('success:', function () {
                            setup(function () {
                                log.args.send[0][5](null, 1977);
                            });

                            test('socket.close was called once', function () {
                                assert.strictEqual(log.counts.close, 1);
                            });

                            test('response.end was called once', function () {
                                assert.strictEqual(log.counts.end, 1);
                            });

                            test('response.end was called correctly', function () {
                                assert.strictEqual(log.args.end[0][0], '{ "sent": 1977 }');
                            });

                            test('response.statusCode was set correctly', function () {
                                assert.strictEqual(response.statusCode, 200);
                            });
                        });
                    });
                });

                suite('receive bad data data:', function () {
                    setup(function () {
                        log.args.on[0][1]('{ "address": "127.0.0.1", "port": 8125, "payload": "This is not a valid request body" }');
                        log.args.on[1][1]();
                    });

                    test('request.socket.destroy called once', function () {
                        assert.strictEqual(log.counts.destroy, 1);
                    });

                    test('request.socket.destroy was called correctly', function () {
                        assert.strictEqual(log.these.destroy[0], request.socket);
                        assert.lengthOf(log.args.destroy[0], 0);
                    });

                    test('response.end was called once', function () {
                        assert.strictEqual(log.counts.end, 1);
                    });

                    test('response.end was called correctly', function () {
                        assert.strictEqual(log.args.end[0][0], '{ "error": "Invalid body" }');
                    });

                    test('response.statusCode was set correctly', function () {
                        assert.strictEqual(response.statusCode, 400);
                    });
                });
            });

            suite('valid request with charset:', function () {
                var request, response;

                setup(function () {
                    request = {
                        url: '/udp',
                        method: 'POST',
                        on: spooks.fn({
                            name: 'on',
                            log: log
                        }),
                        socket: {
                            destroy: spooks.fn({
                                name: 'destroy',
                                log: log
                            })
                        },
                        headers: {
                            'content-type': 'application/x-www-form-urlencoded;charset=UTF-8'
                        }
                    };
                    response = spooks.obj({
                        archetype: { setHeader: nop, end: nop },
                        log: log
                    });
                    log.args.createServer[0][0](request, response);
                });

                teardown(function () {
                    request = response = undefined;
                });

                test('request.on was called twice', function () {
                    assert.strictEqual(log.counts.on, 2);
                });
            });
        });

        suite('call listen with custom options:', function () {
            setup(function () {
                http2udp.listen({
                    host: '192.168.1.1',
                    port: 8080,
                    path: '/foo/bar',
                    origin: 'http://example.com',
                    maxSize: 26,
                    log: spooks.fn({
                        name: 'log',
                        log: log
                    })
                });
            });

            test('http.listen was called correctly', function () {
                assert.strictEqual(log.args.listen[0][0], 8080);
                assert.strictEqual(log.args.listen[0][1], '192.168.1.1');
            });

            test('log was called once', function () {
                assert.strictEqual(log.counts.log, 1);
            });

            test('log was called correctly', function () {
                assert.lengthOf(log.args.log[0], 1);
                assert.strictEqual(log.args.log[0][0], 'http2udp.listen: awaiting POST requests on 192.168.1.1:8080');
            });

            suite('valid request:', function () {
                var request, response;

                setup(function () {
                    request = {
                        url: '/foo/bar',
                        method: 'POST',
                        on: spooks.fn({
                            name: 'on',
                            log: log
                        }),
                        socket: {
                            destroy: spooks.fn({
                                name: 'destroy',
                                log: log
                            })
                        },
                        headers: {
                            'content-type': 'text/plain'
                        }
                    };
                    response = spooks.obj({
                        archetype: { setHeader: nop, end: nop },
                        log: log
                    });
                    log.args.createServer[0][0](request, response);
                });

                teardown(function () {
                    request = response = undefined;
                });

                test('response.setHeader was called correctly second time', function () {
                    assert.strictEqual(log.args.setHeader[1][0], 'Access-Control-Allow-Origin');
                    assert.strictEqual(log.args.setHeader[1][1], 'http://example.com');
                });

                suite('receive 26 characters of data:', function () {
                    setup(function () {
                        log.args.on[0][1]('payload=a&port=2&address=c');
                    });

                    test('request.socket.destroy was not called', function () {
                        assert.strictEqual(log.counts.destroy, 0);
                    });

                    test('response.end was not called', function () {
                        assert.strictEqual(log.counts.end, 0);
                    });

                    test('udp.createSocket was not called', function () {
                        assert.strictEqual(log.counts.createSocket, 0);
                    });

                    test('socket.send was not called', function () {
                        assert.strictEqual(log.counts.send, 0);
                    });

                    test('log was called once', function () {
                        assert.strictEqual(log.counts.log, 1);
                    });

                    suite('receive one character of data:', function () {
                        setup(function () {
                            log.args.on[0][1](' ');
                        });

                        test('request.socket.destroy called once', function () {
                            assert.strictEqual(log.counts.destroy, 1);
                        });

                        test('response.end was called once', function () {
                            assert.strictEqual(log.counts.end, 1);
                        });

                        test('log was called twice', function () {
                            assert.strictEqual(log.counts.log, 2);
                        });

                        test('log was called correctly second time', function () {
                            assert.lengthOf(log.args.log[1], 1);
                            assert.strictEqual(log.args.log[1][0], 'http2udp.fail: 413 Body too large');
                        });
                    });

                    suite('end data:', function () {
                        setup(function () {
                            log.args.on[1][1]();
                        });

                        test('log was called correctly second time', function () {
                            assert.lengthOf(log.args.log[1], 1);
                            assert.strictEqual(log.args.log[1][0], 'http2udp.send: Sending UDP payload to c:2');
                        });

                        test('log was called correctly third time', function () {
                            assert.lengthOf(log.args.log[2], 1);
                            assert.strictEqual(log.args.log[2][0], 'http2udp.send: a');
                        });

                        test('udp.createSocket was called once', function () {
                            assert.strictEqual(log.counts.createSocket, 1);
                        });

                        test('udp.createSocket was called correctly', function () {
                            assert.strictEqual(log.args.createSocket[0][0], 'udp4');
                        });

                        test('socket.send was called once', function () {
                            assert.strictEqual(log.counts.send, 1);
                        });

                        test('socket.send was called correctly', function () {
                            assert.strictEqual(log.args.send[0][1], 0);
                            assert.strictEqual(log.args.send[0][2], 1);
                            assert.strictEqual(log.args.send[0][3], 2);
                            assert.strictEqual(log.args.send[0][4], 'c');
                        });
                    });
                });

                suite('receive non-numeric port data:', function () {
                    setup(function () {
                        log.args.on[0][1]('address=x&port=y&payload=z');
                    });

                    suite('end data:', function () {
                        setup(function () {
                            log.args.on[1][1]();
                        });

                        test('request.socket.destroy called once', function () {
                            assert.strictEqual(log.counts.destroy, 1);
                        });

                        test('response.end was called once', function () {
                            assert.strictEqual(log.counts.end, 1);
                        });

                        test('log was called twice', function () {
                            assert.strictEqual(log.counts.log, 2);
                        });

                        test('log was called correctly second time', function () {
                            assert.lengthOf(log.args.log[1], 1);
                            assert.strictEqual(log.args.log[1][0], 'http2udp.fail: 400 Invalid body');
                        });
                    });
                });
            });

            suite('valid request with charset:', function () {
                var request, response;

                setup(function () {
                    request = {
                        url: '/foo/bar',
                        method: 'POST',
                        on: spooks.fn({
                            name: 'on',
                            log: log
                        }),
                        socket: {
                            destroy: spooks.fn({
                                name: 'destroy',
                                log: log
                            })
                        },
                        headers: {
                            'content-type': 'text/plain; charset=wibble'
                        }
                    };
                    response = spooks.obj({
                        archetype: { setHeader: nop, end: nop },
                        log: log
                    });
                    log.args.createServer[0][0](request, response);
                });

                teardown(function () {
                    request = response = undefined;
                });

                test('request.on was called twice', function () {
                    assert.strictEqual(log.counts.on, 2);
                });
            });
        });

        suite('call listen with multiple origins:', function () {
            setup(function () {
                http2udp.listen({
                    origin: [ 'http://foo', 'https://bar' ],
                    log: spooks.fn({
                        name: 'log',
                        log: log
                    })
                });
            });

            suite('request from first origin:', function () {
                var request, response;

                setup(function () {
                    request = {
                        url: '/udp',
                        method: 'POST',
                        on: function () {},
                        socket: function () {},
                        headers: {
                            origin: 'http://foo',
                            'content-type': 'text/plain'
                        }
                    };
                    response = spooks.obj({
                        archetype: { setHeader: nop, end: nop },
                        log: log
                    });
                    log.args.createServer[0][0](request, response);
                });

                teardown(function () {
                    request = response = undefined;
                });

                test('response.setHeader was called correctly second time', function () {
                    assert.strictEqual(log.args.setHeader[1][1], 'http://foo');
                });
            });

            suite('request from second origin:', function () {
                var request, response;

                setup(function () {
                    request = {
                        url: '/udp',
                        method: 'POST',
                        on: function () {},
                        socket: function () {},
                        headers: {
                            origin: 'https://bar',
                            'content-type': 'text/plain'
                        }
                    };
                    response = spooks.obj({
                        archetype: { setHeader: nop, end: nop },
                        log: log
                    });
                    log.args.createServer[0][0](request, response);
                });

                teardown(function () {
                    request = response = undefined;
                });

                test('response.setHeader was called correctly second time', function () {
                    assert.strictEqual(log.args.setHeader[1][1], 'https://bar');
                });
            });

            suite('request from wrong origin:', function () {
                var request, response;

                setup(function () {
                    request = {
                        url: '/udp',
                        method: 'POST',
                        on: function () {},
                        socket: function () {},
                        headers: {
                            origin: 'http://fooo',
                            'content-type': 'text/plain'
                        }
                    };
                    response = spooks.obj({
                        archetype: { setHeader: nop, end: nop },
                        log: log
                    });
                    log.args.createServer[0][0](request, response);
                });

                teardown(function () {
                    request = response = undefined;
                });

                test('response.setHeader was called correctly second time', function () {
                    assert.strictEqual(log.args.setHeader[1][1], 'null');
                });
            });
        });
    });
});

