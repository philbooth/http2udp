#!/usr/bin/env node

/*globals require, process */

'use strict';

var cli = require('commander'),
    packageInfo = require('../package.json'),
    http2udp = require('./index');

parseCommandLine();
runServer();

function parseCommandLine () {
    cli.version(packageInfo.version)
        .option('-H, --host <hostname>', 'hostname to accept HTTP connections on, default is 0.0.0.0 (INADDR_ANY)')
        .option('-P, --port <port>', 'port to accept HTTP connections on, default is 8008', parseInt)
        .option('-p, --path <path>', 'URL path to accept requests to, default is /udp')
        .option('-o, --origin <origin>', 'Allowed origin(s) for cross-domain requests, comma separated. Default is * (any origin), specify null to force same origin.')
        .option('-m, --maxSize <characters>', 'maximum request body size in characters, defaults to 1024', parseInt)
        .option('-s, --silent', 'prevent the command from logging output to the console')
        .parse(process.argv);
}

function runServer () {
    if (!cli.silent) {
        cli.log = console.log.bind(console);
    }

    if (typeof cli.origin === 'string' && cli.origin.indexOf(',') > -1) {
        cli.origin = cli.origin.split(',');
    }

    http2udp.listen(cli);
}

