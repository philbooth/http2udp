/*globals require, exports, Buffer */

'use strict';

var check = require('check-types'),
    http = require('http'),
    querystring = require('querystring'),
    udp = require('dgram'),

defaults = {
    host: '0.0.0.0',
    port: 8008,
    path: '/udp',
    origin: '*',
    maxSize: 1024,
    log: function () {}
};

exports.listen = listen;

/**
 * Public function `listen`.
 *
 * Awaits HTTP requests, forwarding them to UDP sockets.
 *
 * @option host {string}         HTTP hostname to accept connections on. Defaults to
 *                               '0.0.0.0' (INADDR_ANY).
 * @option port {number}         HTTP port to accept connections on. Defaults to 8008.
 * @option path {string}         URL path to accept requests to. Defaults to '/udp'.
 * @option origin {string|array} URL(s) for the Access-Control-Allow-Origin header.
 * @option maxSize {number}      Maximum request body size in characters. Defaults to
 *                               1024.
 * @option log {function}        Log function that expects a single string argument
 *                               (without terminating newlinne character). Defaults to
 *                               `function () {}`.
 */
function listen (options) {
    var log;

    if (options) {
        verifyOptions(options);
    } else {
        options = {};
    }

    log = getLog(options);

    log('http2udp.listen: awaiting POST requests on ' + getHost(options) + ':' + getPort(options));

    http.createServer(handleRequest.bind(null, log, getPath(options), getOrigin(options), getMaxSize(options)))
        .listen(getPort(options), getHost(options));
}

function verifyOptions (options) {
    check.assert.maybe.unemptyString(options.host, 'Invalid host');
    check.assert.maybe.positive(options.port, 'Invalid port');
    check.assert.maybe.unemptyString(options.path, 'Invalid path');
    check.assert.maybe.positive(options.maxSize, 'Invalid maximum request size');
    check.assert.maybe.function(options.log, 'Invalid log function');

    verifyOrigin(options.origin);
}

function verifyOrigin (origin) {
    if (check.string(origin)) {
        if (origin !== '*' && origin !== 'null') {
            check.assert.webUrl(origin, 'Invalid access control origin');
        }
    } else if (check.array(origin)) {
        origin.forEach(function (o) {
            check.assert.webUrl(o, 'Invalid access control origin');
        });
    } else if (origin) {
        throw new Error('Invalid access control origin');
    }
}

function getLog (options) {
    return getOption('log', options);
}

function getOption (name, options) {
    return options[name] || defaults[name];
}

function getHost (options) {
    return getOption('host', options);
}

function getPort (options) {
    return getOption('port', options);
}

function getPath (options) {
    return getOption('path', options);
}

function getOrigin (options) {
    return getOption('origin', options);
}

function getMaxSize (options) {
    return getOption('maxSize', options);
}

function handleRequest (log, path, origin, maxSize, request, response) {
    var state;

    response.setHeader('Content-Type', 'application/json');
    response.setHeader('Access-Control-Allow-Origin', getAccessControlOrigin(request.headers, origin));

    if (request.method !== 'POST') {
        return fail(log, response, 405, 'Invalid method `' + request.method + '`');
    }

    if (request.url !== path) {
        return fail(log, response, 404, 'Invalid path `' + request.url + '`');
    }

    if (isValidContentType(request.headers['content-type']) === false) {
        return fail(log, response, 415, 'Invalid content type `' + request.headers['content-type'] + '`');
    }

    state = { body: '' };

    request.on('data', receive.bind(null, log, state, maxSize, request, response));
    request.on('end', send.bind(null, log, state, request, response));
}

function getAccessControlOrigin (headers, origin) {
    if (check.array(origin)) {
        if (headers.origin && contains(origin, headers.origin)) {
            return headers.origin;
        }

        return 'null';
    }

    return origin;
}

function isValidContentType (contentType) {
    if (contentType === '') {
        return false;
    }

    if (contentType === 'application/x-www-form-urlencoded' || contentType === 'text/plain') {
        return true;
    }

    return isValidContentType(contentType.substr(0, contentType.indexOf(';')));
}

function contains (array, value) {
    return array.reduce(function (match, candidate) {
        return match || candidate === value;
    }, false);
}

function fail (log, response, status, message) {
    log('http2udp.fail: ' + status + ' ' + message);

    response.statusCode = status;
    response.end('{ "error": "' + message + '" }');
}

function receive (log, state, maxSize, request, response, data) {
    state.body += data;

    if (state.body.length > maxSize) {
        request.socket.destroy();
        state.failed = true;
        return fail(log, response, 413, 'Body too large');
    }
}

function send (log, state, request, response) {
    var data, socket, buffer, port;

    if (state.failed) {
        return;
    }

    try {
        data = querystring.parse(state.body);
        socket = udp.createSocket('udp4');
        buffer = new Buffer(data.payload);
        port = parseInt(data.port);
        if (port !== port) {
            throw new Error();
        }

        log('http2udp.send: Sending UDP payload to ' + data.address + ':' + data.port);
        log('http2udp.send: ' + data.payload);

        socket.send(buffer, 0, buffer.length, port, data.address, function (error, bytes) {
            socket.close();

            if (error) {
                return fail(log, response, 502, error);
            }

            pass(log, response, bytes);
        });
    } catch (error) {
        request.socket.destroy();
        fail(log, response, 400, 'Invalid body');
    }
}

function pass (log, response, bytes) {
    log('http2udp.pass: Sent ' + bytes + ' bytes');

    response.statusCode = 200;
    response.end('{ "sent": ' + bytes + ' }');
}

